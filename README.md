# magnaclima.es

[![magnaclima.es](/assets/media/logo.png)](https://magnaclima.es/)


## STEPS

### Local

- Design
  - IMG + LOGO + FAVICON
    - `assets/media/` folder ➡️ [Compress image tool](https://compressor.io/)
      - `fondo.jpg`
      - `logo.svg`
      - `logo.png`
      - `icon.png`
      - `favicon.ico` ➡️ [Favicon converter tool](https://favicon.io/favicon-converter/)
    - `Gitlab ➡️ Repository ➡️ Settings ➡️ General ➡️ Project avatar` ➡️ change icon
